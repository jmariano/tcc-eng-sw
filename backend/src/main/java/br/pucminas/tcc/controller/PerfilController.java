package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Perfil;
import br.pucminas.tcc.repository.PerfilRepository;

@RestController
@RequestMapping({ "/perfil" })
public class PerfilController {

    final private PerfilRepository perfilRepository;

    PerfilController(PerfilRepository perfilRepository) {
        this.perfilRepository = perfilRepository;
    }

    @GetMapping
    public List<Perfil> listar() {
        return Lists.newArrayList(perfilRepository.findAll());
    }
}