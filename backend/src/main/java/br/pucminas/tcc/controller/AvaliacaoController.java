package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Avaliacao;
import br.pucminas.tcc.service.AvaliacaoService;

@RestController
@RequestMapping({ "/avaliacao" })
public class AvaliacaoController {
    final private AvaliacaoService service;

    AvaliacaoController(AvaliacaoService avaliacaoService) {
        this.service = avaliacaoService;
    }

    @GetMapping
    public List<Avaliacao> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Avaliacao obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Avaliacao inserir(@RequestBody Avaliacao avaliacao) {
        return service.inserir(avaliacao);
    }

    @PutMapping
    public Avaliacao editar(@RequestBody Avaliacao avaliacao) {
        return service.editar(avaliacao);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }

    @GetMapping({ "/aluno/{idAluno}" })
    public List<Avaliacao> listar(@PathVariable("idAluno") Long idAluno) {
        return Lists.newArrayList(service.listarPorMatriculaAluno(idAluno));
    }
}