package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Boletim;
import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.service.BoletimService;

@RestController
@RequestMapping({ "/boletim" })
public class BoletimController {

    final private BoletimService service;

    BoletimController(BoletimService boletimService) {
        this.service = boletimService;
    }

    @GetMapping
    public List<Boletim> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Boletim obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Boletim inserir(@RequestBody Boletim boletim) {
        return service.inserir(boletim);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }

    @GetMapping({ "/{id}/nota" })
    public List<Nota> listarNotas(@PathVariable("id") Long id) {
        return service.listarNotas(id);
    }
}