package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Matricula;
import br.pucminas.tcc.service.MatriculaService;

@RestController
@RequestMapping({ "/matricula" })
public class MatriculaController {

    final private MatriculaService service;

    public MatriculaController(MatriculaService matriculaService) {
        this.service = matriculaService;
    }

    @GetMapping
    public List<Matricula> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Matricula obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Matricula inserir(@RequestBody Matricula matricula) {
        return service.inserir(matricula);
    }

    @PutMapping
    public Matricula editar(@RequestBody Matricula matricula) {
        return service.editar(matricula);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }
}