package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Curso;
import br.pucminas.tcc.service.CursoService;

@RestController
@RequestMapping({ "/curso" })
public class CursoController {

    final private CursoService service;

    public CursoController(CursoService cursoService) {
        this.service = cursoService;
    }

    @GetMapping
    public List<Curso> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Curso obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Curso inserir(@RequestBody Curso curso) {
        return service.inserir(curso);
    }

    @PutMapping
    public Curso editar(@RequestBody Curso curso) {
        return service.editar(curso);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }

    @GetMapping({ "/aluno/{idAluno}" })
    public List<Curso> listarPorAlunoMatriculado(@PathVariable("idAluno") Long idAluno) {
        return Lists.newArrayList(service.listarPorAlunoMatriculado(idAluno));
    }
}