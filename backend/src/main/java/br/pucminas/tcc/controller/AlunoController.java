package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.service.AlunoService;

@RestController
@RequestMapping({ "/aluno" })
public class AlunoController {
    final private AlunoService service;

    AlunoController(AlunoService alunoService) {
        this.service = alunoService;
    }

    @GetMapping
    public List<Aluno> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Aluno obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Aluno inserir(@RequestBody Aluno aluno) {
        return service.inserir(aluno);
    }

    @PutMapping
    public Aluno editar(@RequestBody Aluno aluno) {
        return service.editar(aluno);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }

    @PostMapping({ "/novo" })
    public Aluno registrar(@RequestBody Aluno aluno) {
        return service.inserir(aluno);
    }
}