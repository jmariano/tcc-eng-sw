package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.service.NotaService;

@RestController
@RequestMapping({ "/nota" })
public class NotaController {

    final private NotaService service;

    NotaController(NotaService notaService) {
        this.service = notaService;
    }

    @GetMapping
    public List<Nota> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Nota obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Nota inserir(@RequestBody Nota nota) {
        return service.inserir(nota);
    }

    @PutMapping
    public Nota editar(@RequestBody Nota nota) {
        return service.editar(nota);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }
}