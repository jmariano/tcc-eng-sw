package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Usuario;
import br.pucminas.tcc.service.UsuarioService;

@RestController
@RequestMapping({ "/usuario" })
public class UsuarioController {

    final private UsuarioService service;

    UsuarioController(UsuarioService usuarioService) {
        this.service = usuarioService;
    }

    @GetMapping
    public List<Usuario> listar() {
        return Lists.newArrayList(service.listar());
    }

    @GetMapping({ "/{id}" })
    public Usuario obter(@PathVariable("id") Long id) {
        return service.obter(id);
    }

    @PostMapping
    public Usuario inserir(@RequestBody Usuario usuario) {
        return service.inserir(usuario);
    }

    @PutMapping
    public Usuario editar(@RequestBody Usuario usuario) {
        return service.editar(usuario);
    }

    @DeleteMapping({ "/{id}" })
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }
}