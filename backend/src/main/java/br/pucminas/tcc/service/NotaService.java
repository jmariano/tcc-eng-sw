package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.AvaliacaoRepository;
import br.pucminas.tcc.repository.NotaRepository;

@Service
public class NotaService {
    final private AlunoRepository alunoRepository;
    final private AvaliacaoRepository avaliacaoRepository;
    final private NotaRepository notaRepository;
    final private SecurityService securityService;

    public NotaService(AlunoRepository alunoRepository, AvaliacaoRepository avaliacaoRepository,
            NotaRepository notaRepository, SecurityService securityService) {
        this.alunoRepository = alunoRepository;
        this.avaliacaoRepository = avaliacaoRepository;
        this.notaRepository = notaRepository;
        this.securityService = securityService;
    }

    public Nota inserir(Nota nota) {
        nota.setAluno(alunoRepository.findById(nota.getAluno().getCodigoAluno()).get());
        nota.setAvaliacao(avaliacaoRepository.findById(nota.getAvaliacao().getCodigoAvaliacao()).get());

        return notaRepository.save(nota);
    }

    public Nota editar(Nota notaEditada) {
        Nota nota = notaRepository.findById(notaEditada.getId()).get();
        nota.setNota(notaEditada.getNota());
        return notaRepository.save(nota);
    }

    public Iterable<Nota> listar() {
        if (securityService.isAdmin()) {
            return notaRepository.findAll();
        }

        return notaRepository.findAllByAluno(securityService.getAluno());
    }

    public Nota obter(Long id) {
        return notaRepository.findById(id).orElse(null);
    }

    public void excluir(Long id) {
        notaRepository.delete(obter(id));
    }
}