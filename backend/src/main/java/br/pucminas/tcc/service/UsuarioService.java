package br.pucminas.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Usuario;
import br.pucminas.tcc.repository.UsuarioRepository;
import br.pucminas.tcc.security.UsuarioAutenticado;

@Service
public class UsuarioService implements UserDetailsService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null) {
            throw new UsernameNotFoundException(email);
        }
        return new UsuarioAutenticado(usuario);
    }

    public Usuario inserir(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Usuario editar(Usuario usuario) {
        Usuario usuarioAtual = obter(usuario.getId());
        if (usuario.getSenha() == null) {
            usuario.setSenha(usuarioAtual.getSenha());
        }
        return usuarioRepository.save(usuario);
    }

    public Iterable<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    public Usuario obter(Long id) {
        return usuarioRepository.findById(id).orElse(null);
    }

    public void excluir(Long id) {
        usuarioRepository.delete(obter(id));
    }

}