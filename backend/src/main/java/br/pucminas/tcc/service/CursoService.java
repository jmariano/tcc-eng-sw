package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Curso;
import br.pucminas.tcc.repository.CursoRepository;

@Service
public class CursoService {
    final private CursoRepository cursoRepository;

    public CursoService(CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    public Curso inserir(Curso curso) {
        return cursoRepository.save(curso);
    }

    public Curso editar(Curso curso) {
        return cursoRepository.save(curso);
    }

    public Iterable<Curso> listar() {
        return cursoRepository.findAll();
    }

    public Curso obter(Long id) {
        return cursoRepository.findById(id).orElse(null);
    }

    public void excluir(Long id) {
        cursoRepository.delete(obter(id));
    }

    public Iterable<Curso> listarPorAlunoMatriculado(Long idAluno) {
        return cursoRepository.listarPorAlunoMatriculado(idAluno);
    }
}