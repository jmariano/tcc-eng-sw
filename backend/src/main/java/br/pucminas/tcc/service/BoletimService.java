package br.pucminas.tcc.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Boletim;
import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.repository.BoletimRepository;
import br.pucminas.tcc.repository.NotaRepository;
import io.jsonwebtoken.lang.Collections;

@Service
public class BoletimService {
    final private NotaRepository notaRepository;
    final private BoletimRepository boletimRepository;
    final private SecurityService securityService;

    public BoletimService(NotaRepository notaRepository, BoletimRepository boletimRepository,
            SecurityService securityService) {
        this.notaRepository = notaRepository;
        this.boletimRepository = boletimRepository;
        this.securityService = securityService;
    }

    public Boletim inserir(Boletim boletim) {
        List<Nota> notas = notaRepository.findByAlunoCurso(boletim.getAluno(), boletim.getCurso());

        boletim.setNotas(notas);
        if (Collections.isEmpty(notas)) {
            boletim.setNotaFinal(0d);
        } else {
            boletim.setNotaFinal(notas.stream().mapToDouble(Nota::getNota).sum() / notas.size());
        }
        boletim.setSituacao(boletim.getNotaFinal() >= 60d ? "Aprovado" : "Reprovado");

        return boletimRepository.save(boletim);
    }

    public Iterable<Boletim> listar() {
        if (securityService.isAdmin()) {
            return boletimRepository.findAll();
        }

        return boletimRepository.findAllByAluno(securityService.getAluno());
    }

    public Boletim obter(Long id) {
        return boletimRepository.findById(id).orElse(null);
    }

    public void excluir(Long id) {
        boletimRepository.delete(obter(id));
    }

    public List<Nota> listarNotas(Long idBoletim) {
        Boletim boletim = boletimRepository.findById(idBoletim).get();
        return notaRepository.findByAlunoCurso(boletim.getAluno(), boletim.getCurso());
    }
}