package br.pucminas.tcc.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Matricula;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.CursoRepository;
import br.pucminas.tcc.repository.MatriculaRepository;

@Service
public class MatriculaService {
    final private MatriculaRepository matriculaRepository;
    final private AlunoRepository alunoRepository;
    final private CursoRepository cursoRepository;
    final private SecurityService securityService;

    public MatriculaService(MatriculaRepository matriculaRepository, AlunoRepository alunoRepository,
            CursoRepository cursoRepository, SecurityService securityService) {
        this.matriculaRepository = matriculaRepository;
        this.alunoRepository = alunoRepository;
        this.cursoRepository = cursoRepository;
        this.securityService = securityService;
    }

    public Iterable<Matricula> listar() {
        if (securityService.isAdmin()) {
            return matriculaRepository.findAll();
        }

        return matriculaRepository.findAllByAluno(securityService.getAluno());
    }

    public Matricula obter(Long id) {
        return matriculaRepository.findById(id).orElse(null);
    }

    public Matricula inserir(Matricula matricula) {
        if (matricula.getAluno() == null) {
            matricula.setAluno(securityService.getAluno());
        }
        matricula.setCurso(cursoRepository.findById(matricula.getCurso().getCodigoCurso()).get());
        matricula.setData(new Date());

        return matriculaRepository.save(matricula);
    }

    public Matricula editar(Matricula matricula) {
        return matriculaRepository.save(matricula);
    }

    public void excluir(Long id) {
        matriculaRepository.delete(obter(id));
    }
}