package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Usuario;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.PerfilRepository;
import br.pucminas.tcc.repository.UsuarioRepository;
import io.jsonwebtoken.lang.Collections;

@Service
public class AlunoService {
    final private AlunoRepository alunoRepository;
    final private UsuarioRepository usuarioRepository;
    final private PerfilRepository perfilRepository;

    public AlunoService(AlunoRepository alunoRepository, UsuarioRepository usuarioRepository,
            PerfilRepository perfilRepository) {
        this.alunoRepository = alunoRepository;
        this.usuarioRepository = usuarioRepository;
        this.perfilRepository = perfilRepository;
    }

    public Aluno inserir(Aluno aluno) {
        criarUsuario(aluno.getUsuario());

        return alunoRepository.save(aluno);
    }

    private void criarUsuario(Usuario usuario) {
        if (Collections.isEmpty(usuario.getPerfis())) {
            usuario.getPerfis().add(perfilRepository.findByNome(SecurityService.ALUNO));
        }
        usuarioRepository.save(usuario);
    }

    public Aluno editar(Aluno aluno) {
        Usuario usuarioEditado = aluno.getUsuario();
        aluno.setUsuario(usuarioRepository.findByEmail(aluno.getEmail()));
        if (usuarioEditado.getSenha() != null) {
            aluno.getUsuario().setSenha(usuarioEditado.getSenha());
        }
        return alunoRepository.save(aluno);
    }

    public Iterable<Aluno> listar() {
        return alunoRepository.findAll();
    }

    public Aluno obter(Long id) {
        return alunoRepository.findById(id).orElse(null);
    }

    public void excluir(Long id) {
        alunoRepository.delete(obter(id));
    }
}