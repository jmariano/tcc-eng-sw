package br.pucminas.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Perfil;
import br.pucminas.tcc.model.Usuario;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.UsuarioRepository;

@Service
public class SecurityService {
    public final static String ALUNO = "ALUNO";
    public final static String ADMIN = "ADMIN";

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    AlunoRepository alunoRepository;

    public Usuario getUsuario() {
        return usuarioRepository.findByEmail(getUsuarioAutenticado());
    }

    public String getUsuarioAutenticado() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public Aluno getAluno() {
        return alunoRepository.findByEmail(getUsuario().getEmail()).get();
    }

    public boolean isAdmin() {
        return getUsuario().getPerfis().stream().map(Perfil::getNome).anyMatch(p -> p.equals(ADMIN));
    }
}