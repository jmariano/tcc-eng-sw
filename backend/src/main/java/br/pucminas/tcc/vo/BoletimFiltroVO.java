package br.pucminas.tcc.vo;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Curso;

public class BoletimFiltroVO {
    private Aluno aluno;
    private Curso curso;

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
}