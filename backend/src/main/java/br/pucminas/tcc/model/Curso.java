package br.pucminas.tcc.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

@Entity
public class Curso implements Serializable {
    private static final long serialVersionUID = -1722711525810558179L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigoCurso;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String anoSemestre;

    @JsonIgnore
    @OneToMany(mappedBy = "curso")
    private Set<Matricula> matriculas;

    @JsonIgnore
    @OneToMany(mappedBy = "curso")
    private Set<Avaliacao> avaliacoes;

    @JsonIgnore
    @OneToMany(mappedBy = "curso")
    private Set<Boletim> boletins;

    public Long getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(Long codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAnoSemestre() {
        return anoSemestre;
    }

    public void setAnoSemestre(String anoSemestre) {
        this.anoSemestre = anoSemestre;
    }

    public Set<Matricula> getMatriculas() {
        return matriculas;
    }

    public void setMatriculas(Set<Matricula> matriculas) {
        this.matriculas = matriculas;
    }

    public Set<Avaliacao> getAvaliacoes() {
        return avaliacoes;
    }

    public void setAvaliacoes(Set<Avaliacao> avaliacoes) {
        this.avaliacoes = avaliacoes;
    }

    public Set<Boletim> getBoletins() {
        return boletins;
    }

    public void setBoletins(Set<Boletim> boletins) {
        this.boletins = boletins;
    }
}
