package br.pucminas.tcc.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Nota implements Serializable {
    private static final long serialVersionUID = -556435004339201882L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Double nota;

    @ManyToOne
    @JoinColumn(name = "codigoAluno")
    private Aluno aluno;

    @ManyToOne
    @JoinColumn(name = "codigoAvaliacao")
    private Avaliacao avaliacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }
}
