package br.pucminas.tcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Boletim;

public interface BoletimRepository extends JpaRepository<Boletim, Long> {

	Iterable<Boletim> findAllByAluno(Aluno aluno);

}
