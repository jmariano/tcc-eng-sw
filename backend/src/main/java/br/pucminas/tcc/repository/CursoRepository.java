package br.pucminas.tcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.pucminas.tcc.model.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    @Query("select c from Curso c join c.matriculas m where m.aluno.id = :idAluno")
	Iterable<Curso> listarPorAlunoMatriculado(@Param("idAluno") Long idAluno);
}
