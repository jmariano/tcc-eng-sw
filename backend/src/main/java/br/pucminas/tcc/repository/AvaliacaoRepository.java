package br.pucminas.tcc.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.pucminas.tcc.model.Avaliacao;

public interface AvaliacaoRepository extends CrudRepository<Avaliacao, Long> {
    @Query("select m from Avaliacao m join Matricula a on a.curso = m.curso where a.aluno.id = :idAluno")
    Iterable<Avaliacao> findAllByMatriculaAluno(@Param("idAluno") Long idAluno);
}
