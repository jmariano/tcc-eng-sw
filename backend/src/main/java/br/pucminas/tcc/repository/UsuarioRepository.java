package br.pucminas.tcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pucminas.tcc.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByEmail(String username);
}