package br.pucminas.tcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.pucminas.tcc.model.Perfil;

public interface PerfilRepository extends JpaRepository<Perfil, Long> {

	Perfil findByNome(String nome);

}