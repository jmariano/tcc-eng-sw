import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from '../services/env.service';
import { Storage } from '@ionic/storage';
import * as jwt_decode from 'jwt-decode';
import { Role } from './role';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any;
  perfis: any;
  constructor(
    private http: HttpClient,
    private env: EnvService,
    private storage: Storage
  ) { }
  login(email: String, password: String) {
    return this.http.post(`${this.env.API_URL}/login`, { username: email, password: password }, { observe: 'response' })
      .pipe(
        tap((res: any) => {
          if (res.status === 200) {
            this.storage.set("token", res.headers.get('authorization'));
            this.token = res.headers.get('authorization');
            this.isLoggedIn = true;
            this.perfis = this.recuperaPerfis();

            return this.token;
          }
        }),
      );
  }
  register(fName: String, lName: String, email: String, password: String) {
    return this.http.post(this.env.API_URL + '/register',
      { fName: fName, lName: lName, email: email, password: password }
    )
  }
  logout() {
    this.isLoggedIn = false;
    delete this.token;
    return this.storage.remove("token");
  }
  user() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"] + " " + this.token["access_token"]
    });
    return this.http.get(this.env.API_URL + '/user', { headers: headers })
      .pipe(
        tap(user => {
          return user;
        })
      )
  }
  getToken() {
    return this.storage.get('token').then(
      data => {
        this.token = data;
        if (this.token != null) {
          this.isLoggedIn = true;
          this.perfis = this.recuperaPerfis();
        } else {
          this.isLoggedIn = false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }
  getCachedToken() {
    return this.token;
  }

  isAdmin() {
    return this.perfis && this.perfis.indexOf(Role.Admin) !== -1
  }

  isAluno() {
    return this.perfis && this.perfis.indexOf(Role.Aluno) !== -1
  }

  private recuperaPerfis() {
    const decoded_token = jwt_decode(this.token);

    return decoded_token.rol;
  }
}