import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { NgForm, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { AlunoService } from 'src/app/aluno/aluno.service';
import { estados } from 'src/app/aluno/estados';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
  providers: [AlunoService]
})
export class RegisterPage {
  alunoForm: FormGroup;
  aluno: any;
  estados: any;

  constructor(private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private alunoService: AlunoService,
    private formBuilder: FormBuilder
  ) {
    this.aluno = {};
    this.createForm();
    this.estados = estados;
  }

  createForm() {
    this.alunoForm = this.formBuilder.group({
      nome: [this.aluno.nome, Validators.required],
      cpf: [this.aluno.cpf, Validators.required],
      endereco: [this.aluno.endereco, Validators.required],
      municipio: [this.aluno.municipio, Validators.required],
      estado: [this.aluno.estado, Validators.required],
      email: [this.aluno.email, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      telefone: [this.aluno.telefone, Validators.required],
      senha: [this.aluno.senha, Validators.required]
    });
  }

  fechar() {
    this.modalController.dismiss();
  }

  cadastrar() {
    this.alunoForm.value.usuario = { email: this.alunoForm.value.email, senha: this.alunoForm.value.senha };
    this.alunoService.criarConta(this.alunoForm.value).then(
      () => {
        this.authService.login(this.alunoForm.value.email, this.alunoForm.value.senha).subscribe(
          () => {},
          () => {
            this.fechar();
            this.alertService.presentToast('Conta criada com sucesso!');
          },
          () => {
            this.fechar();
            this.navCtrl.navigateRoot('/home');
          }
        );
        this.alertService.presentToast('Conta criada com sucesso!');
      },
      error => {
        this.alertService.presentToast('Não é possível criar a conta!');
      }
    );
  }
}