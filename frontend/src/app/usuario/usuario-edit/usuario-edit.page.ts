import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/usuario/usuario.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-usuario-edit',
  templateUrl: './usuario-edit.page.html',
  styleUrls: ['./usuario-edit.page.scss'],
  providers: [UsuarioService]
})
export class UsuarioEditPage implements OnInit {
  title: string;
  usuarioForm: FormGroup;
  usuario: any;
  perfis: any;
  perfisSelecionados: any;

  constructor(
    private usuarioService: UsuarioService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams) {

    this.usuario = this.navParams.get('usuario');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
    this.listarPerfis();
  }

  createForm() {
    const campos = {
      id: [this.usuario.id],
      email: [this.usuario.email, Validators.required],
      senha: [null],
      perfis: [this.usuario.perfis, Validators.required]
    };

    if (!this.isEdicao()) {
      campos['senha'] = [null, Validators.required];
    }

    this.usuarioForm = this.formBuilder.group(campos);
  }

  private setupPageTitle() {
    this.title = this.usuario.id ? 'Editar usuário' : 'Incluir usuário';
  }

  onSubmit() {
    if (this.usuarioForm.valid) {
      this.usuarioForm.value.perfis = this.perfisSelecionados.filter((p: any) => p.checked);
      let servico: any;
      if (!this.usuarioForm.value.id) {
        servico = this.usuarioService.incluir(this.usuarioForm.value);
      } else {
        servico = this.usuarioService.alterar(this.usuarioForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar o usuário.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  listarPerfis() {
    this.perfis = this.usuarioService.listarPerfis();
    this.perfis.then((perfis) => {
      perfis = perfis.map((p) => {
        p.checked = this.usuario.perfis.find((pf) => pf.nome === p.nome) !== undefined;
        return p;
      });
      this.perfisSelecionados = perfis;
    })
  }

  adicionarPerfil(perfil: any, ev: any) {
    let p = this.perfisSelecionados.find((pf) => pf.nome === perfil.nome);
    p.checked = this.usuarioForm.value.perfis;
  }

  isEdicao() {
    return this.usuario.id !== null && this.usuario.id !== undefined;
  }
}
