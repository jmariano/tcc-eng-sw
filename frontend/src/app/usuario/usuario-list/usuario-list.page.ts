import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { UsuarioEditPage } from '../usuario-edit/usuario-edit.page';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.page.html',
  styleUrls: ['./usuario-list.page.scss'],
  providers: [UsuarioService]
})
export class UsuarioListPage {

  private usuarios: any;

  constructor(private usuarioservice: UsuarioService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController) { }

  private listarUsuarios() {
    this.usuarios = this.usuarioservice.listar();
  }

  ionViewWillEnter() {
    this.listarUsuarios();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: UsuarioEditPage,
      componentProps: {
        usuario: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarUsuarios());
    return await modal.present();
  }

  async editar(usuario: any) {
    const modal = await this.modalCtrl.create({
      component: UsuarioEditPage,
      componentProps: {
        usuario: usuario
      }
    });
    modal.onDidDismiss().then(() => this.listarUsuarios());
    return await modal.present();
  }

  excluir(usuario: any) {
    this.usuarioservice.excluir(usuario.id).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarUsuarios();
    }, () => {
      this.alertService.presentToast("Não é possível excluir o usuario.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }

}
