import { Component, OnInit } from '@angular/core';
import { CursoService } from '../curso.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-curso-edit',
  templateUrl: './curso-edit.page.html',
  styleUrls: ['./curso-edit.page.scss'],
  providers: [CursoService]
})
export class CursoEditPage implements OnInit {
  title: string;
  cursoForm: FormGroup;
  curso: any;

  constructor(
    private cursoService: CursoService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams) {

    this.curso = this.navParams.get('curso');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
  }

  createForm() {
    this.cursoForm = this.formBuilder.group({
      codigoCurso: [this.curso.codigoCurso],
      nome: [this.curso.nome, Validators.required],
      anoSemestre: [this.curso.anoSemestre, Validators.required]
    });
  }

  private setupPageTitle() {
    this.title = this.curso.codigoCurso ? 'Editar curso' : 'Incluir curso';
  }

  onSubmit() {
    if (this.cursoForm.valid) {
      let servico;
      if (!this.cursoForm.value.codigoCurso) {
        servico = this.cursoService.incluir(this.cursoForm.value);
      } else {
        servico = this.cursoService.alterar(this.cursoForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar o curso.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }
}
