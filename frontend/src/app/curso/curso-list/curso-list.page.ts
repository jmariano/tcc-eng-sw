import { Component } from '@angular/core';
import { CursoService } from '../curso.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { CursoEditPage } from '../curso-edit/curso-edit.page';

@Component({
  selector: 'app-curso-list',
  templateUrl: './curso-list.page.html',
  styleUrls: ['./curso-list.page.scss'],
  providers: [CursoService]
})
export class CursoListPage {
  private cursos: any;

  constructor(private cursoservice: CursoService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController) { }

  private listarCursos() {
    this.cursos = this.cursoservice.listar();
  }

  ionViewWillEnter() {
    this.listarCursos();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: CursoEditPage,
      componentProps: {
        curso: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarCursos());
    return await modal.present();
  }

  async editar(curso: any) {
    const modal = await this.modalCtrl.create({
      component: CursoEditPage,
      componentProps: {
        curso: curso
      }
    });
    modal.onDidDismiss().then(() => this.listarCursos());
    return await modal.present();
  }

  excluir(curso: any) {
    this.cursoservice.excluir(curso.codigoCurso).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarCursos();
    }, () => {
      this.alertService.presentToast("Não é possível excluir o curso.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }
}
