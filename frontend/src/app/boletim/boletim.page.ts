import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

import { AlunoService } from '../aluno/aluno.service';
import { CursoService } from '../curso/curso.service';
import { BoletimService } from './boletim.service';

@Component({
  selector: 'app-boletim',
  templateUrl: './boletim.page.html',
  styleUrls: ['./boletim.page.scss'],
})
export class BoletimPage implements OnInit {

  constructor(
    private alunoService: AlunoService,
    private cursoService: CursoService,
    private boletimService: BoletimService,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) { }

  alunos: any;
  avaliacoes: any;
  cursos: any;
  boletim: any;

  exibeBoletim: boolean;

  ngOnInit() {
    this.listAlunos();
    this.listCursos();

    this.exibeBoletim = false;
  }

  compareWith = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  listAlunos() {
    this.alunos = this.alunoService.listar();
  }

  listCursos() {
    this.cursos = this.cursoService.listar();
  }

  emitir(form) {
    this.emitirBoletim(form.value)
      .then((result: any) => {
        this.exibeBoletim = true;
        this.boletim = result;
      },
        () => {
          this.showToast('Erro ao emitir boletim de notas.')
        });
  }

  voltar(form) {
    form.reset();
    this.exibeBoletim = false;
  }

  isAprovado(resultado) {
    return resultado === 'Aprovado'
  }

  private emitirBoletim(boletim: any) {
    return this.boletimService.incluir(boletim);
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  cancelar() {
    this.navCtrl.navigateRoot('/home');
  }
}
