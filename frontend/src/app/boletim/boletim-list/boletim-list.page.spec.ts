import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoletimListPage } from './boletim-list.page';

describe('BoletimListPage', () => {
  let component: BoletimListPage;
  let fixture: ComponentFixture<BoletimListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoletimListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoletimListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
