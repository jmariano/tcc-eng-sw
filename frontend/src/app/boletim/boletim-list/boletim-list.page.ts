import { Component, OnInit } from '@angular/core';
import { BoletimService } from '../boletim.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { BoletimEditPage } from '../boletim-edit/boletim-edit.page';

@Component({
  selector: 'app-boletim-list',
  templateUrl: './boletim-list.page.html',
  styleUrls: ['./boletim-list.page.scss'],
})
export class BoletimListPage {
  private boletins: any;

  constructor(private boletimService: BoletimService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private authService: AuthService) { }

  private listarBoletims() {
    this.boletins = this.boletimService.listar();
  }

  ionViewWillEnter() {
    this.listarBoletims();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: BoletimEditPage,
      componentProps: {
        boletim: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarBoletims());
    return await modal.present();
  }

  async editar(boletim: any) {
    const modal = await this.modalCtrl.create({
      component: BoletimEditPage,
      componentProps: {
        boletim: boletim
      }
    });
    modal.onDidDismiss().then(() => this.listarBoletims());
    return await modal.present();
  }

  excluir(boletim: any) {
    this.boletimService.excluir(boletim.id).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarBoletims();
    }, () => {
      this.alertService.presentToast("Não é possível excluir o boletim.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

}
