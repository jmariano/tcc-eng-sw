import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlunoService } from 'src/app/aluno/aluno.service';
import { BoletimService } from '../boletim.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { CursoService } from 'src/app/curso/curso.service';

@Component({
  selector: 'app-boletim-edit',
  templateUrl: './boletim-edit.page.html',
  styleUrls: ['./boletim-edit.page.scss'],
})
export class BoletimEditPage implements OnInit {
  title: string;
  boletimForm: FormGroup;
  boletim: any;
  alunos: any;
  cursos: any;

  constructor(
    private alunoService: AlunoService,
    private cursoService: CursoService,
    private boletimService: BoletimService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams,
    private authService: AuthService) {

    this.boletim = this.navParams.get('boletim');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
    this.listAlunos();
    this.listCursos();
  }

  ionViewWillEnter() {
    if (this.boletim && this.boletim.id) {
      this.listNotas();
    }
  }

  compareWithAluno = (o1, o2) => {
    return o1 && o2 ? o1.codigoAluno === o2.codigoAluno : o1 === o2;
  }

  compareWithAvaliacao = (o1, o2) => {
    return o1 && o2 ? o1.codigoAvaliacao === o2.codigoAvaliacao : o1 === o2;
  }

  createForm() {
    const campos = {
      id: [this.boletim.id],
      curso: [this.boletim.curso, Validators.required]
    };

    if (this.isAdmin()) {
      campos['aluno'] = [this.boletim.aluno, Validators.required];
    }

    this.boletimForm = this.formBuilder.group(campos);
  }

  private setupPageTitle() {
    this.title = this.isEdicao() ? 'Visualizar boletim' : 'Incluir boletim';
  }

  onSubmit() {
    if (this.boletimForm.valid) {
      this.boletimService.incluir(this.boletimForm.value).then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar o boletim.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  listAlunos() {
    this.alunos = this.alunoService.listar();
  }

  listCursos() {
    if (!this.boletimForm.value.aluno || !this.boletimForm.value.aluno.codigoAluno) {
      this.cursos = null;
      return;
    }
    this.cursos = this.cursoService.listarPorMatriculaAluno(this.boletimForm.value.aluno.codigoAluno);
  }

  listNotas() {
    this.boletim.notas = this.boletimService.listarNotas(this.boletim.id);
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

  isEdicao() {
    return this.boletim.id !== null && this.boletim.id !== undefined;
  }

  voltar() {
    this.cancelar();
  }

  isAprovado(resultado) {
    return resultado === 'Aprovado'
  }

  recuperaCursos(aluno: any) {
    if (this.boletimForm.value.aluno) {
      this.cursos = this.cursoService.listarPorMatriculaAluno(this.boletimForm.value.aluno.codigoAluno);
    } else {
      this.cursos = null;
    }
  }

}
