import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatriculaEditPage } from './matricula-edit.page';

describe('MatriculaEditPage', () => {
  let component: MatriculaEditPage;
  let fixture: ComponentFixture<MatriculaEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatriculaEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatriculaEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
