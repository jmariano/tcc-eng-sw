import { Component, OnInit } from '@angular/core';
import { CursoService } from 'src/app/curso/curso.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';
import { AlunoService } from 'src/app/aluno/aluno.service';
import { MatriculaService } from '../matricula.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-matricula-edit',
  templateUrl: './matricula-edit.page.html',
  styleUrls: ['./matricula-edit.page.scss'],
  providers: [CursoService, AlunoService, MatriculaService]
})
export class MatriculaEditPage implements OnInit {
  title: string;
  matriculaForm: FormGroup;
  matricula: any;
  cursos: any;
  alunos: any;

  constructor(
    private cursoService: CursoService,
    private alunoService: AlunoService,
    private matriculaService: MatriculaService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams,
    private authService: AuthService) {

    this.matricula = this.navParams.get('matricula');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
    this.listCursos();
    this.listAlunos();
  }

  compareWithCurso = (o1, o2) => {
    return o1 && o2 ? o1.codigoCurso === o2.codigoCurso : o1 === o2;
  }

  compareWithAluno = (o1, o2) => {
    return o1 && o2 ? o1.codigoAluno === o2.codigoAluno : o1 === o2;
  }

  createForm() {
    const campos = {
      codigoMatricula: [this.matricula.codigoMatricula],
      curso: [{ value: this.matricula.curso, disabled: this.isEdicao() }, Validators.required]
    };

    if (this.isAdmin()) {
      campos['aluno'] = [{ value: this.matricula.aluno, disabled: this.isEdicao() }, Validators.required];
    }

    this.matriculaForm = this.formBuilder.group(campos);
  }

  private setupPageTitle() {
    this.title = this.matricula.codigoMatricula ? 'Editar matricula' : 'Incluir matricula';
  }

  onSubmit() {
    if (this.matriculaForm.valid) {
      let servico;
      if (!this.matriculaForm.value.codigoMatricula) {
        servico = this.matriculaService.incluir(this.matriculaForm.value);
      } else {
        servico = this.matriculaService.alterar(this.matriculaForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar a matricula.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  listCursos() {
    this.cursos = this.cursoService.listar();
  }

  listAlunos() {
    this.alunos = this.alunoService.listar();
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

  isEdicao() {
    return this.matricula.codigoMatricula !== null && this.matricula.codigoMatricula !== undefined;
  }
}
