import { Component, OnInit } from '@angular/core';
import { MatriculaService } from '../matricula.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { MatriculaEditPage } from '../matricula-edit/matricula-edit.page';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-matricula-list',
  templateUrl: './matricula-list.page.html',
  styleUrls: ['./matricula-list.page.scss'],
  providers: [MatriculaService]
})
export class MatriculaListPage {
  private matriculas: any;

  constructor(private matriculaService: MatriculaService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private authService: AuthService) { }

  private listarMatriculas() {
    this.matriculas = this.matriculaService.listar();
  }

  ionViewWillEnter() {
    this.listarMatriculas();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: MatriculaEditPage,
      componentProps: {
        matricula: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarMatriculas());
    return await modal.present();
  }

  async editar(matricula: any) {
    const modal = await this.modalCtrl.create({
      component: MatriculaEditPage,
      componentProps: {
        matricula: matricula
      }
    });
    modal.onDidDismiss().then(() => this.listarMatriculas());
    return await modal.present();
  }

  excluir(matricula: any) {
    this.matriculaService.excluir(matricula.codigoMatricula).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarMatriculas();
    }, () => {
      this.alertService.presentToast("Não é possível excluir a matricula.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }

  isAdmin() {
    return this.authService.isAdmin();
  }
}
