import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatriculaListPage } from './matricula-list.page';

describe('MatriculaListPage', () => {
  let component: MatriculaListPage;
  let fixture: ComponentFixture<MatriculaListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatriculaListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatriculaListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
