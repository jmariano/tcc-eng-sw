import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CursoService } from '../curso/curso.service';
import { ToastController, NavController } from '@ionic/angular';
import { MatriculaService } from './matricula.service';

@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.page.html',
  styleUrls: ['./matricula.page.scss'],
  providers: [CursoService, MatriculaService]
})

export class MatriculaPage implements OnInit {
  constructor(
    private cursoService: CursoService,
    private matriculaService: MatriculaService,
    private toastCtrl: ToastController,
    private router: Router,
    private navCtrl: NavController) { }

  cursos: any;

  compareWith = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  listCursos() {
    this.cursos = this.cursoService.listar();
  }

  save(form) {
    this.salvarMatricula(form.value)
      .then(() => {
        this.showToast('Matricula realizada com sucesso.')
        this.router.navigate(['home'])
      })
      .catch((error) => {
        this.showToast('Erro ao matricular aluno. ' + error.error)
      })
  }

  private salvarMatricula(matricula: any) {
    return this.matriculaService.incluir(matricula);
  }

  ngOnInit() {
    this.listCursos();
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  cancelar() {
    this.navCtrl.navigateRoot('/home');
  }
}
