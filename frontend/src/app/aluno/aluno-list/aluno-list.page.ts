import { Component, OnInit } from '@angular/core';
import { AlunoService } from 'src/app/aluno/aluno.service';
import { AlunoEditPage } from '../aluno-edit/aluno-edit.page';
import { ModalController, NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-aluno-list',
  templateUrl: './aluno-list.page.html',
  styleUrls: ['./aluno-list.page.scss'],
  providers: [AlunoService]
})
export class AlunoListPage {
  private alunos: any;

  constructor(
    private alunoService: AlunoService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController) { }

  private listarAlunos() {
    this.alunos = this.alunoService.listar();
  }

  ionViewWillEnter() {
    this.listarAlunos();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: AlunoEditPage,
      componentProps: {
        aluno: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarAlunos());
    return await modal.present();
  }

  async editar(aluno: any) {
    const modal = await this.modalCtrl.create({
      component: AlunoEditPage,
      componentProps: {
        aluno: aluno
      }
    });
    modal.onDidDismiss().then(() => this.listarAlunos());
    return await modal.present();
  }

  excluir(aluno: any) {
    this.alunoService.excluir(aluno.codigoAluno).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarAlunos();
    }, () => {
      this.alertService.presentToast("Não é possível excluir o aluno.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }
}
