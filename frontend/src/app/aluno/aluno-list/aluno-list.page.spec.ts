import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunoListPage } from './aluno-list.page';

describe('AlunoListPage', () => {
  let component: AlunoListPage;
  let fixture: ComponentFixture<AlunoListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlunoListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
