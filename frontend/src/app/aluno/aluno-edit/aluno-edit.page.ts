import { Component, OnInit } from '@angular/core';

import { NavController, ModalController, NavParams } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlunoService } from '../aluno.service'
import { AlertService } from 'src/app/services/alert.service';
import { estados } from '../estados';

@Component({
  selector: 'app-aluno-edit',
  templateUrl: './aluno-edit.page.html',
  styleUrls: ['./aluno-edit.page.scss'],
  providers: [AlunoService]
})
export class AlunoEditPage implements OnInit {
  title: string;
  alunoForm: FormGroup;
  aluno: any;
  estados: any;

  constructor(
    private alunoService: AlunoService,
    private alertService: AlertService,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams) {

    this.aluno = this.navParams.get('aluno');

    this.createForm();
    this.setupPageTitle();
    this.estados = estados;
  }

  ngOnInit() {
  }

  createForm() {
    const campos = {
      codigoAluno: [this.aluno.codigoAluno],
      nome: [this.aluno.nome, Validators.required],
      cpf: [this.aluno.cpf, Validators.required],
      endereco: [this.aluno.endereco, Validators.required],
      municipio: [this.aluno.municipio, Validators.required],
      estado: [this.aluno.estado, Validators.required],
      email: [this.aluno.email, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      telefone: [this.aluno.telefone, Validators.required],
      senha: [this.aluno.senha, Validators.required]
    };
    if (this.isEdicao) {
      campos['senha'] = [this.aluno.senha];
    }
    this.alunoForm = this.formBuilder.group(campos);
  }

  private setupPageTitle() {
    this.title = this.isEdicao() ? 'Editar aluno' : 'Incluir aluno';
  }

  onSubmit() {
    if (this.alunoForm.valid) {
      this.alunoForm.value.usuario = { email: this.alunoForm.value.email, senha: this.alunoForm.value.senha };
      let servico;
      if (!this.alunoForm.value.codigoAluno) {
        servico = this.alunoService.incluir(this.alunoForm.value);
      } else {
        servico = this.alunoService.alterar(this.alunoForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar o aluno.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  isEdicao() {
    return this.aluno.codigoAluno !== null && this.aluno.codigoAluno !== undefined;
  }
}
