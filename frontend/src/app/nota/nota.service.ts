import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { EnvService } from '../services/env.service';

@Injectable({
  providedIn: 'root'
})
export class NotaService {
  private headers: HttpHeaders;
  private API_URL: string;

  constructor(private http: HttpClient, private authService: AuthService, private env: EnvService) {
    this.headers = new HttpHeaders({
      'Authorization': this.authService.getCachedToken()
    });
    this.API_URL = `${this.env.API_URL}/nota`;
  }

  incluir(nota: any) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL;

      this.http.post(url, nota, { headers: this.headers })
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }

  alterar(nota: any) {
    return new Promise((resolve, reject) => {
      let url = this.API_URL;

      this.http.put(url, nota, { headers: this.headers })
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }

  obter(id: any) {
    return new Promise((resolve, reject) => {
      let url = `${this.API_URL}/${id}`;

      this.http.get(url, { headers: this.headers })
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }

  listar() {
    return new Promise((resolve, reject) => {
      this.http.get(this.API_URL, { headers: this.headers })
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }

  excluir(id: any) {
    return new Promise((resolve, reject) => {
      let url = `${this.API_URL}/${id}`;

      this.http.delete(url, { headers: this.headers })
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }
}
