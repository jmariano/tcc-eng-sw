import { Component, OnInit } from '@angular/core';
import { NotaService } from '../nota.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { NotaEditPage } from '../nota-edit/nota-edit.page';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-nota-list',
  templateUrl: './nota-list.page.html',
  styleUrls: ['./nota-list.page.scss'],
  providers: [NotaService]
})
export class NotaListPage {

  private notas: any;

  constructor(private notaService: NotaService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private authService: AuthService) { }

  private listarNotas() {
    this.notas = this.notaService.listar();
  }

  ionViewWillEnter() {
    this.listarNotas();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: NotaEditPage,
      componentProps: {
        nota: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarNotas());
    return await modal.present();
  }

  async editar(nota: any) {
    const modal = await this.modalCtrl.create({
      component: NotaEditPage,
      componentProps: {
        nota: nota
      }
    });
    modal.onDidDismiss().then(() => this.listarNotas());
    return await modal.present();
  }

  excluir(nota: any) {
    this.notaService.excluir(nota.id).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarNotas();
    }, () => {
      this.alertService.presentToast("Não é possível excluir a nota.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

}
