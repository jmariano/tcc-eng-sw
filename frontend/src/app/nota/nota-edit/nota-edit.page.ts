import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlunoService } from 'src/app/aluno/aluno.service';
import { AvaliacaoService } from 'src/app/avaliacao/avaliacao.service';
import { NotaService } from '../nota.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-nota-edit',
  templateUrl: './nota-edit.page.html',
  styleUrls: ['./nota-edit.page.scss'],
})
export class NotaEditPage implements OnInit {
  title: string;
  notaForm: FormGroup;
  nota: any;
  alunos: any;
  avaliacoes: any;

  constructor(
    private alunoService: AlunoService,
    private avaliacaoService: AvaliacaoService,
    private notaService: NotaService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams,
    private authService: AuthService) {

    this.nota = this.navParams.get('nota');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
    this.listAlunos();
    this.listAvaliacoes();
  }

  compareWithAluno = (o1, o2) => {
    return o1 && o2 ? o1.codigoAluno === o2.codigoAluno : o1 === o2;
  }

  compareWithAvaliacao = (o1, o2) => {
    return o1 && o2 ? o1.codigoAvaliacao === o2.codigoAvaliacao : o1 === o2;
  }

  createForm() {
    this.notaForm = this.formBuilder.group({
      id: [this.nota.id],
      nota: [this.nota.nota, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]{1,3}(\.[0-9]{1,2})?$')
      ])],
      avaliacao: [{ value: this.nota.avaliacao, disabled: this.isEdicao() }, Validators.required],
      aluno: [{ value: this.nota.aluno, disabled: this.isEdicao() }, Validators.required]
    });
  }

  private setupPageTitle() {
    this.title = this.isEdicao() ? 'Editar nota' : 'Incluir nota';
  }

  onSubmit() {
    if (this.notaForm.valid) {
      let servico;
      if (!this.notaForm.value.id) {
        servico = this.notaService.incluir(this.notaForm.value);
      } else {
        servico = this.notaService.alterar(this.notaForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar a nota.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  listAlunos() {
    this.alunos = this.alunoService.listar();
  }

  listAvaliacoes() {
    if (!this.nota.aluno) {
      this.avaliacoes = null;
      return;
    }
    this.avaliacoes = this.avaliacaoService.listarPorMatriculaAluno(this.nota.aluno.codigoAluno);
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

  isEdicao() {
    return this.nota.id !== null && this.nota.id !== undefined;
  }

  carregarAvaliacoes() {
    if (this.notaForm.value.aluno) {
      this.avaliacoes = this.avaliacaoService.listarPorMatriculaAluno(this.notaForm.value.aluno.codigoAluno);
    } else {
      this.avaliacoes = null;
    }
  }
}
