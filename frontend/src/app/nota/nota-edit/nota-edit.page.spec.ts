import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaEditPage } from './nota-edit.page';

describe('NotaEditPage', () => {
  let component: NotaEditPage;
  let fixture: ComponentFixture<NotaEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotaEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
