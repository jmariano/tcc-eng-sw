import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';

import { AvaliacaoService } from '../avaliacao/avaliacao.service';
import { AlunoService } from '../aluno/aluno.service';
import { NotaService } from './nota.service';

@Component({
  selector: 'app-nota',
  templateUrl: './nota.page.html',
  styleUrls: ['./nota.page.scss'],
  providers: [AvaliacaoService, AlunoService, NotaService]
})
export class NotaPage implements OnInit {

  constructor(
    private avaliacaoService: AvaliacaoService,
    private alunoService: AlunoService,
    private notaService: NotaService,
    private toastCtrl: ToastController,
    private router: Router,
    private navCtrl: NavController
  ) { }

  alunos: any;
  avaliacoes: any;

  ngOnInit() {
    this.listAlunos();
    this.listAvaliacoes();
  }

  compareWith = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  listAlunos() {
    this.alunos = this.alunoService.listar();
  }

  listAvaliacoes() {
    this.avaliacoes = this.avaliacaoService.listar();
  }

  save(form) {
    this.salvarNota(form.value)
      .then(() => {
        this.showToast('Nota registrada com sucesso.')
        this.router.navigate(['home'])
      })
      .catch((error) => {
        this.showToast('Erro ao registrar nota.')
      })
  }

  private salvarNota(nota: any) {
    return this.notaService.incluir(nota);
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  cancelar() {
    this.navCtrl.navigateRoot('/home');
  }
}
