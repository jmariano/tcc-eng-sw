import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages: any;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private authService: AuthService,
    private navCtrl: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.authService.getToken();
      this.appPages = this.getPages();
    });
  }

  logout() {
    this.authService.logout().then(() => {
      this.navCtrl.navigateRoot('/landing');
    }, (error) => {
      console.log(error);
    });
  }

  getPages() {
    return new Promise((resolve, reject) => {
      this.authService.getToken().then(() => {
        let pages = [];
        if (this.authService.isAdmin()) {
          pages = [
            {
              title: 'Ínicio',
              url: '/home',
              icon: 'home'
            },
            {
              title: 'Alunos',
              url: '/aluno-list',
              icon: 'people'
            },
            {
              title: 'Cursos',
              url: '/curso-list',
              icon: 'clipboard'
            },
            {
              title: 'Matriculas',
              url: '/matricula-list',
              icon: 'albums'
            },
            {
              title: 'Avaliações',
              url: '/avaliacao-list',
              icon: 'book'
            },
            {
              title: 'Notas',
              url: '/nota-list',
              icon: 'browsers'
            },
            {
              title: 'Boletim de notas',
              url: '/boletim-list',
              icon: 'calculator'
            },
            {
              title: 'Usuários',
              url: '/usuario-list',
              icon: 'contacts'
            }
          ];
        } else if (this.authService.isAluno()) {
          pages = [
            {
              title: 'Ínicio',
              url: '/home',
              icon: 'home'
            },
            {
              title: 'Matriculas',
              url: '/matricula-list',
              icon: 'albums'
            },
            {
              title: 'Notas',
              url: '/nota-list',
              icon: 'browsers'
            },
            {
              title: 'Boletim de notas',
              url: '/boletim-list',
              icon: 'calculator'
            }
          ];
        } else {
          pages = [{
            title: 'Ínicio',
            url: '/home',
            icon: 'home'
          }];
        }
        resolve(pages)
      }, (error) => {
        reject(error);
      })
    });
  }
}
