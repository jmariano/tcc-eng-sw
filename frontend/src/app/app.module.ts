import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AlunoEditPageModule } from './aluno/aluno-edit/aluno-edit.module';
import { CursoEditPageModule } from './curso/curso-edit/curso-edit.module';
import { MatriculaEditPageModule } from './matricula/matricula-edit/matricula-edit.module';
import { AvaliacaoEditPageModule } from './avaliacao/avaliacao-edit/avaliacao-edit.module';
import { NotaEditPageModule } from './nota/nota-edit/nota-edit.module';
import { UsuarioEditPageModule } from './usuario/usuario-edit/usuario-edit.module';
import { BoletimEditPageModule } from './boletim/boletim-edit/boletim-edit.module';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AlunoEditPageModule,
    CursoEditPageModule,
    MatriculaEditPageModule,
    AvaliacaoEditPageModule,
    NotaEditPageModule,
    UsuarioEditPageModule,
    BoletimEditPageModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
