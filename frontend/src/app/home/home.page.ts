import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(private authService: AuthService, private navCtrl: NavController) { }

  logout() {
    this.authService.logout().then(() => {
      this.navCtrl.navigateRoot('/landing');
    }, (error) => {
      console.log(error);
    });
  }

  isAdmin() {
    return this.authService.isAdmin();
  }
}
