import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { Role } from './auth/role';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  { path: 'landing', loadChildren: './landing/landing.module#LandingPageModule' },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'matricula',
    canActivate: [AuthGuard],
    loadChildren: './matricula/matricula.module#MatriculaPageModule',
    data: { roles: [Role.Aluno] }
  },
  {
    path: 'nota',
    canActivate: [AuthGuard],
    loadChildren: './nota/nota.module#NotaPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'boletim',
    canActivate: [AuthGuard],
    loadChildren: './boletim/boletim.module#BoletimPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'avaliacao',
    canActivate: [AuthGuard],
    loadChildren: './avaliacao/avaliacao.module#AvaliacaoPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'aluno-list',
    canActivate: [AuthGuard],
    loadChildren: './aluno/aluno-list/aluno-list.module#AlunoListPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'avaliacao-list',
    canActivate: [AuthGuard],
    loadChildren: './avaliacao/avaliacao-list/avaliacao-list.module#AvaliacaoListPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'matricula-list',
    canActivate: [AuthGuard],
    loadChildren: './matricula/matricula-list/matricula-list.module#MatriculaListPageModule',
    data: { roles: [Role.Admin, Role.Aluno] }
  },
  {
    path: 'nota-list',
    canActivate: [AuthGuard],
    loadChildren: './nota/nota-list/nota-list.module#NotaListPageModule',
    data: { roles: [Role.Admin, Role.Aluno] }
  },
  {
    path: 'usuario-list',
    canActivate: [AuthGuard],
    loadChildren: './usuario/usuario-list/usuario-list.module#UsuarioListPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'curso-list',
    canActivate: [AuthGuard],
    loadChildren: './curso/curso-list/curso-list.module#CursoListPageModule',
    data: { roles: [Role.Admin] }
  },
  {
    path: 'boletim-list',
    canActivate: [AuthGuard],
    loadChildren: './boletim/boletim-list/boletim-list.module#BoletimListPageModule',
    data: { roles: [Role.Admin, Role.Aluno] }
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
