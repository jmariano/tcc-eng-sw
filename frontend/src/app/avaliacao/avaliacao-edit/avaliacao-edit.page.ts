import { Component, OnInit } from '@angular/core';
import { CursoService } from 'src/app/curso/curso.service';
import { AvaliacaoService } from '../avaliacao.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-avaliacao-edit',
  templateUrl: './avaliacao-edit.page.html',
  styleUrls: ['./avaliacao-edit.page.scss'],
  providers: [CursoService, AvaliacaoService]
})
export class AvaliacaoEditPage implements OnInit {
  title: string;
  avaliacaoForm: FormGroup;
  avaliacao: any;
  cursos: any;
  alunos: any;

  constructor(
    private cursoService: CursoService,
    private avaliacaoService: AvaliacaoService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private navParams: NavParams) {

    this.avaliacao = this.navParams.get('avaliacao');

    this.createForm();
    this.setupPageTitle();
  }

  ngOnInit() {
    this.listCursos();
  }

  compareWithCurso = (o1, o2) => {
    return o1 && o2 ? o1.codigoCurso === o2.codigoCurso : o1 === o2;
  }

  createForm() {
    this.avaliacaoForm = this.formBuilder.group({
      codigoAvaliacao: [this.avaliacao.codigoAvaliacao],
      nome: [this.avaliacao.nome, Validators.required],
      data: [this.avaliacao.data, Validators.required],
      curso: [this.avaliacao.curso, Validators.required]
    });
  }

  private setupPageTitle() {
    this.title = this.avaliacao.codigoAvaliacao ? 'Editar avaliacao' : 'Incluir avaliacao';
  }

  onSubmit() {
    if (this.avaliacaoForm.valid) {
      let servico;
      if (!this.avaliacaoForm.value.codigoAvaliacao) {
        servico = this.avaliacaoService.incluir(this.avaliacaoForm.value);
      } else {
        servico = this.avaliacaoService.alterar(this.avaliacaoForm.value);
      }
      servico.then(() => {
        this.modalController.dismiss();
        this.alertService.presentToast("Salvo com sucesso.");
      })
        .catch((e) => {
          this.alertService.presentToast("Não é possível salvar a avaliação.");
          console.error(e);
        })
    }
  }

  cancelar() {
    this.modalController.dismiss();
  }

  listCursos() {
    this.cursos = this.cursoService.listar();
  }
}
