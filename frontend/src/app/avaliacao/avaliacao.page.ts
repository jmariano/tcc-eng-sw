import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CursoService } from '../curso/curso.service';
import { ToastController, NavController } from '@ionic/angular';
import { AvaliacaoService } from './avaliacao.service';

@Component({
  selector: 'app-avaliacao',
  templateUrl: './avaliacao.page.html',
  styleUrls: ['./avaliacao.page.scss'],
  providers: [CursoService, AvaliacaoService]
})
export class AvaliacaoPage implements OnInit {

  constructor(
    private cursoService: CursoService,
    private avaliacaoService: AvaliacaoService,
    private toastCtrl: ToastController,
    private router: Router,
    private navCtrl: NavController
  ) { }
  
  cursos: any;

  ngOnInit() {
    this.listCursos();
  }

  compareWith = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  listCursos() {
   this.cursos = this.cursoService.listar();
  }

  save(form) {
    this.salvarAvaliacao(form.value)
      .then(() => {
        this.showToast('Avaliação cadastrada com sucesso.')
        this.router.navigate(['home'])
      })
      .catch((error) => {
        this.showToast('Erro ao cadastrar avaliação.')
      })
  }
 
  private salvarAvaliacao(avaliacao: any) {
      return this.avaliacaoService.incluir(avaliacao);
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }

  cancelar() {
    this.navCtrl.navigateRoot('/home');
  }
}
