import { Component, OnInit } from '@angular/core';
import { AvaliacaoService } from '../avaliacao.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { AvaliacaoEditPage } from 'src/app/avaliacao/avaliacao-edit/avaliacao-edit.page';

@Component({
  selector: 'app-avaliacao-list',
  templateUrl: './avaliacao-list.page.html',
  styleUrls: ['./avaliacao-list.page.scss'],
  providers: [AvaliacaoService]
})
export class AvaliacaoListPage {

  private avaliacoes: any;

  constructor(private avaliacaoService: AvaliacaoService,
    private alertService: AlertService,
    private modalCtrl: ModalController,
    private navCtrl: NavController) { }

  private listarAvaliacoes() {
    this.avaliacoes = this.avaliacaoService.listar();
  }

  ionViewWillEnter() {
    this.listarAvaliacoes();
  }

  async adicionar() {
    const modal = await this.modalCtrl.create({
      component: AvaliacaoEditPage,
      componentProps: {
        avaliacao: {}
      }
    });
    modal.onDidDismiss().then(() => this.listarAvaliacoes());
    return await modal.present();
  }

  async editar(avaliacao: any) {
    const modal = await this.modalCtrl.create({
      component: AvaliacaoEditPage,
      componentProps: {
        avaliacao: avaliacao
      }
    });
    modal.onDidDismiss().then(() => this.listarAvaliacoes());
    return await modal.present();
  }

  excluir(avaliacao: any) {
    this.avaliacaoService.excluir(avaliacao.codigoAvaliacao).then(() => {
      this.alertService.presentToast("Excluido com sucesso.");
      this.listarAvaliacoes();
    }, () => {
      this.alertService.presentToast("Não é possível excluir a avaliacao.");
    });
  }

  voltar() {
    this.navCtrl.navigateRoot('/home');
  }
}
