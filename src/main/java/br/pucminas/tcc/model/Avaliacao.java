package br.pucminas.tcc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Avaliacao implements Serializable {
    private static final long serialVersionUID = -7418108684478424658L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigoAvaliacao;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private Date data;

    @ManyToOne
    @JoinColumn(name = "codigoCurso")
    private Curso curso;

    @JsonIgnore
    @OneToMany(mappedBy = "avaliacao")
    private Set<Nota> notas;

    public Long getCodigoAvaliacao() {
        return codigoAvaliacao;
    }

    public void setCodigoAvaliacao(Long codigoAvaliacao) {
        this.codigoAvaliacao = codigoAvaliacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Set<Nota> getNotas() {
        return notas;
    }

    public void setNotas(Set<Nota> notas) {
        this.notas = notas;
    }
}
