package br.pucminas.tcc.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Matricula implements Serializable {
    private static final long serialVersionUID = -8181695300875648623L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codigoMatricula;

    @Column(nullable = false)
    private Date data;

    @ManyToOne
    @JoinColumn(name = "cpf", referencedColumnName = "cpf")
    private Aluno aluno;

    @ManyToOne
    @JoinColumn(name = "codigoCurso")
    private Curso curso;

    public Long getCodigoMatricula() {
        return codigoMatricula;
    }

    public void setCodigoMatricula(Long codigoMatricula) {
        this.codigoMatricula = codigoMatricula;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @PrePersist
    public void defineData() {
        setData(new Date());
    }
}
