package br.pucminas.tcc.vo;

import java.util.List;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Curso;
import br.pucminas.tcc.model.Nota;

public class BoletimVO {
    private List<Nota> notas;
    private Curso curso;
    private Aluno aluno;
    private Double notaFinal;
    private String resultado;

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(Double notaFinal) {
        this.notaFinal = notaFinal;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}