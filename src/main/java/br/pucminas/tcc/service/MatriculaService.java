package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Matricula;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.CursoRepository;
import br.pucminas.tcc.repository.MatriculaRepository;

@Service
public class MatriculaService {
    final private MatriculaRepository matriculaRepository;
    final private AlunoRepository alunoRepository;
    final private CursoRepository cursoRepository;

    public MatriculaService(MatriculaRepository matriculaRepository, AlunoRepository alunoRepository, CursoRepository cursoRepository) {
        this.matriculaRepository = matriculaRepository;
        this.alunoRepository = alunoRepository;
        this.cursoRepository = cursoRepository;
    }

    public Matricula salvar(Matricula matricula) {
        matricula.setAluno(alunoRepository.findById(1l).get());
        matricula.setCurso(cursoRepository.findById(matricula.getCurso().getCodigoCurso()).get());

        return matriculaRepository.save(matricula);
    }
}