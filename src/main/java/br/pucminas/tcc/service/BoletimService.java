package br.pucminas.tcc.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.repository.NotaRepository;
import br.pucminas.tcc.vo.BoletimFiltroVO;
import br.pucminas.tcc.vo.BoletimVO;

@Service
public class BoletimService {
    final private NotaRepository notaRepository;

    public BoletimService(NotaRepository notaRepository) {
        this.notaRepository = notaRepository;
    }

    public BoletimVO emitir(BoletimFiltroVO filtro) {
        List<Nota> notas = notaRepository.findByAlunoCurso(filtro.getAluno(), filtro.getCurso());

        BoletimVO boletimVO = new BoletimVO();
        boletimVO.setAluno(filtro.getAluno());
        boletimVO.setCurso(filtro.getCurso());
        boletimVO.setNotas(notas);
        boletimVO.setNotaFinal(notas.stream().mapToDouble(Nota::getNota).sum() / notas.size());
        boletimVO.setResultado(boletimVO.getNotaFinal() >= 60d ? "Aprovado" : "Reprovado");
        return boletimVO;
    }
}