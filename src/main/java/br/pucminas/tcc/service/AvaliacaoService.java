package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Avaliacao;
import br.pucminas.tcc.repository.AvaliacaoRepository;
import br.pucminas.tcc.repository.CursoRepository;

@Service
public class AvaliacaoService {
    final private AvaliacaoRepository avaliacaoRepository;
    final private CursoRepository cursoRepository;

    public AvaliacaoService(AvaliacaoRepository avaliacaoRepository, CursoRepository cursoRepository) {
        this.avaliacaoRepository = avaliacaoRepository;
        this.cursoRepository = cursoRepository;
    }

    public Avaliacao salvar(Avaliacao avaliacao) {
        avaliacao.setCurso(cursoRepository.findById(avaliacao.getCurso().getCodigoCurso()).get());

        return avaliacaoRepository.save(avaliacao);
    }

    public Iterable<Avaliacao> listar() {
        return avaliacaoRepository.findAll();
    }
}