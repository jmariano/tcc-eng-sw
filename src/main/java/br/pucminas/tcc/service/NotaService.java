package br.pucminas.tcc.service;

import org.springframework.stereotype.Service;

import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.repository.AlunoRepository;
import br.pucminas.tcc.repository.AvaliacaoRepository;
import br.pucminas.tcc.repository.NotaRepository;

@Service
public class NotaService {
    final private AlunoRepository alunoRepository;
    final private AvaliacaoRepository avaliacaoRepository;
    final private NotaRepository notaRepository;

    public NotaService(AlunoRepository alunoRepository, AvaliacaoRepository avaliacaoRepository,
            NotaRepository notaRepository) {
        this.alunoRepository = alunoRepository;
        this.avaliacaoRepository = avaliacaoRepository;
        this.notaRepository = notaRepository;
    }

    public Nota salvar(Nota nota) {
        nota.setAluno(alunoRepository.findById(1l).get());
        nota.setAvaliacao(avaliacaoRepository.findById(nota.getAvaliacao().getCodigoAvaliacao()).get());

        return notaRepository.save(nota);
    }
}