package br.pucminas.tcc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.pucminas.tcc.model.Aluno;
import br.pucminas.tcc.model.Curso;
import br.pucminas.tcc.model.Nota;

@RepositoryRestResource(collectionResourceRel = "nota", path = "nota")
public interface NotaRepository extends JpaRepository<Nota, Long> {
    
    @Query("select n from Nota n where n.aluno = :aluno and n.avaliacao.curso = :curso")
	List<Nota> findByAlunoCurso(@Param("aluno") Aluno aluno, @Param("curso") Curso curso);
}
