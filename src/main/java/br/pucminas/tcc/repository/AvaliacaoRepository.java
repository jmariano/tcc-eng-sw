package br.pucminas.tcc.repository;

import org.springframework.data.repository.CrudRepository;

import br.pucminas.tcc.model.Avaliacao;

public interface AvaliacaoRepository extends CrudRepository<Avaliacao, Long> {

}
