package br.pucminas.tcc.repository;

import br.pucminas.tcc.model.Matricula;
import org.springframework.data.repository.CrudRepository;

public interface MatriculaRepository extends CrudRepository<Matricula, Long> {
}
