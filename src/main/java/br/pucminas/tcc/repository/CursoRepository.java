package br.pucminas.tcc.repository;

import br.pucminas.tcc.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "curso", path = "curso")
public interface CursoRepository extends JpaRepository<Curso, Long> {
}
