package br.pucminas.tcc.repository;

import br.pucminas.tcc.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "aluno", path = "aluno")
public interface AlunoRepository extends JpaRepository<Aluno, Long> {
}
