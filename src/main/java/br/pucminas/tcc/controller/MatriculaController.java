package br.pucminas.tcc.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Matricula;
import br.pucminas.tcc.service.MatriculaService;

@RestController
@RequestMapping({ "/matricula" })
public class MatriculaController {

    final private MatriculaService service;

    MatriculaController(MatriculaService matriculaService) {
        this.service = matriculaService;
    }

    @PostMapping
    public Matricula create(@RequestBody Matricula matricula) {
        return service.salvar(matricula);
    }
}