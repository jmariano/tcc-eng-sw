package br.pucminas.tcc.controller;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Avaliacao;
import br.pucminas.tcc.service.AvaliacaoService;

@RestController
@RequestMapping({ "/avaliacao" })
public class AvaliacaoController {
    final private AvaliacaoService service;

    AvaliacaoController(AvaliacaoService avaliacaoService) {
        this.service = avaliacaoService;
    }

    @PostMapping
    public Avaliacao create(@RequestBody Avaliacao avaliacao) {
        return service.salvar(avaliacao);
    }

    @GetMapping
    public List<Avaliacao> findAll() {
        return Lists.newArrayList(service.listar());
    }
}