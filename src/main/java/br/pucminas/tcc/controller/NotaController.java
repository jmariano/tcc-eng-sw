package br.pucminas.tcc.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.model.Nota;
import br.pucminas.tcc.service.NotaService;

@RestController
@RequestMapping({ "/nota" })
public class NotaController {

    final private NotaService service;

    NotaController(NotaService notaService) {
        this.service = notaService;
    }

    @PostMapping
    public Nota create(@RequestBody Nota nota) {
        return service.salvar(nota);
    }
}