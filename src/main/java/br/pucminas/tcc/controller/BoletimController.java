package br.pucminas.tcc.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.pucminas.tcc.service.BoletimService;
import br.pucminas.tcc.vo.BoletimFiltroVO;
import br.pucminas.tcc.vo.BoletimVO;

@RestController
@RequestMapping({ "/boletim" })
public class BoletimController {

    final private BoletimService service;

    BoletimController(BoletimService boletimService) {
        this.service = boletimService;
    }

    @PostMapping
    public BoletimVO emitir(@RequestBody BoletimFiltroVO filtro) {
        return service.emitir(filtro);
    }
}